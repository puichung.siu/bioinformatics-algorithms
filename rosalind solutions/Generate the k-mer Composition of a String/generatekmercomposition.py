#!/usr/bin/env/python3

"""
Name = Siu Pui Chung Jacky
Student number = 1047527
Script for algorithm for bioinformatics preperation
input: integer k and string Text
output: composition of all kmers of text
"""

from sys import argv

def composition(k, string):
    comp = []
    n = len(string)
    for i in range(n - k + 1): #sliding window
        comp.append(string[i: i+k])#size as kmer size
    return comp
    
if __name__ == "__main__":
    filename = argv[1]
    with open(filename, 'r') as f:
        lines = f.readlines()
    kmer = int(lines[0].strip())
    string = lines[1].strip()
    comp = composition(kmer, string)
    f = open("answer.txt", "w")
    f.write("\n".join(comp))
    f.close()
